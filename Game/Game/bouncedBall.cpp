//#include <easyx.h>
//#include <stdio.h>
//#include <conio.h>
//#include <time.h>
//
//// ball结构体
//typedef struct {
//    int circleX, circleY;
//    int r;
//    int stepX, stepY;
//}BallData;
//
//// rectangle结构体
//typedef struct {
//    int rectangleX1;
//    int rectangleX2;
//    int rectangleY1;
//    int rectangleY2;
//}RectangleData;
//
//// 设置小球状态
//void setBall(BallData* balldata)
//{
//    // 位置
//    balldata->circleX = rand() % (500 + 1) - 225;
//    balldata->circleY = rand() % (200 + 1);
//    // 速度
//    balldata->stepX = 10;
//    balldata->stepY = 10;
//    // 方向
//    if (balldata->circleX % 2 == 1) {
//        balldata->stepX = -(balldata->stepX);
//    }
//    if (balldata->circleY % 2 == 1) {
//        balldata->stepY = -(balldata->stepY);
//    }
//    // 半径
//    balldata->r = 30;
//}
//
//int main()
//{
//    // 创建并设置背景颜色绘图窗口
//    initgraph(1000, 800);
//    setbkcolor(RGB(255, 128, 128));
//    cleardevice();
//
//    // 设置逻辑坐标原点并翻转逻辑坐标轴向
//    setorigin(500, 400);
//    setaspectratio(1, -1);
//
//    srand((unsigned int)time(NULL));
//
//    // 绘制一个会移动的蓝色、碰到边界和矩形会反弹的小球
//    // 绘制一个黄色的、可以移动的矩形
//    BallData ballData;
//    setBall(&ballData);
//    RectangleData rectangleData;
//    rectangleData.rectangleX1 = -150;
//    rectangleData.rectangleX2 = 150;
//    rectangleData.rectangleY1 = -300;
//    rectangleData.rectangleY2 = -400;
//
//    while (1)
//    {
//        // 绘ball
//        cleardevice();
//        setfillcolor(RGB(128, 179, 255));
//        solidcircle(ballData.circleX, ballData.circleY, 30);
//        ballData.circleX += ballData.stepX;
//        ballData.circleY += ballData.stepY;
//
//        // 绘rectangle
//        setfillcolor(RGB(255, 230, 128));
//        solidrectangle(rectangleData.rectangleX1, rectangleData.rectangleY1, rectangleData.rectangleX2, rectangleData.rectangleY2);
//
//        Sleep(40);
//
//        // ball反弹
//        if (ballData.circleX >= 485 || ballData.circleX <= -485)
//        {
//            ballData.stepX = -ballData.stepX;
//        }
//        if (ballData.circleY >= 385) {
//            ballData.stepY = -ballData.stepY;
//        }
//        if (ballData.circleY <= -285 && ballData.circleX >= rectangleData.rectangleX1 && ballData.circleX <= rectangleData.rectangleX2)
//        {
//            ballData.stepY = -ballData.stepY;
//        }
//
//        // 控制矩形
//        if (_kbhit() != 0) {
//            char c = _getch();
//            if (c == 'a' && rectangleData.rectangleX1 >= -480)
//            {
//                rectangleData.rectangleX1 -= 20;
//                rectangleData.rectangleX2 -= 20;
//            }
//            if (c == 'd' && rectangleData.rectangleX2 <= 480)
//            {
//                rectangleData.rectangleX1 += 20;
//                rectangleData.rectangleX2 += 20;
//            }
//        }
//
//        // 结束游戏
//        if (ballData.circleY <= -400)
//        {
//            break;
//        }
//    }
//
//
//    closegraph();
//    return 0;
//}