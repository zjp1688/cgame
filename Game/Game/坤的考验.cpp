#define _CRT_SECURE_NO_WARNINGS
#include <easyx.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>

// 定义熊图有11张
#define BEAR_FRAMES 11

// 定义绘制图片函数
void putTransparentImage(int x, int y, const IMAGE* mask, const IMAGE* img)
{
	putimage(x, y, mask, SRCAND);
	putimage(x, y, img, SRCPAINT);
}


// ********蛇********
// node结构体(蛇蛇和食物)
typedef struct {
	int x;
	int y;
}node;

// 蛇蛇移动方向
enum direction {
	up,
	down,
	left,
	right,
};

// 定义网格的宽度为20
#define NODE_WIDTH 20

// 定义绘制蛇蛇函数
void drawSnake(node* snake, int length) {
	int left = 0;
	int top = 0;
	int right = 0;
	int bottom = 0;
	setfillcolor(WHITE);
	for (int i = 0; i < length; i++) {
		left = snake[i].x * NODE_WIDTH;
		top = snake[i].y * NODE_WIDTH;
		right = (snake[i].x + 1) * NODE_WIDTH;
		bottom = (snake[i].y + 1) * NODE_WIDTH;
		solidrectangle(left, top, right, bottom);
	}
}

// 定义蛇蛇移动函数
node snakeMove(node* snake, int length, int direction) {
	node tail = snake[length - 1];

	// body
	for (int i = length - 1; i > 0; i--) {
		snake[i] = snake[i - 1];
	}

	// head
	node newhead;
	newhead = snake[0];
	if (direction == left) {
		newhead.x--;
	}
	else if (direction == right) {
		newhead.x++;
	}
	else if (direction == up) {
		newhead.y--;
	}
	else {
		newhead.y++;
	}
	snake[0] = newhead;
	return tail;
}

// 定义控制蛇蛇方向函数
void changeDirection(enum direction* d) {
	if (_kbhit() != 0) {
		char ch = _getch();
		switch (ch) {
		case 'w':
			if (*d != up && *d != down) {
				*d = up;
			}
			break;
		case 's':
			if (*d != up && *d != down) {
				*d = down;
			}
			break;
		case 'a':
			if (*d != right && *d != left) {
				*d = left;
			}
		case 'd':
			if (*d != right && *d != left) {
				*d = right;
			}
		}

	}
}

// 定义蛇蛇Over函数
bool judgmentGameOver(node* snake, int length) {
	// border
	if (snake[0].x <= 0 || snake[0].x >= (1000 / NODE_WIDTH) + 1) {
		return true;
	}
	if (snake[0].y <= 0 || snake[0].y >= (800 / NODE_WIDTH) + 1) {
		return true;
	}

	// body
	for (int i = 1; i < length; i++) {
		if (snake[i].x == snake[0].x && snake[i].y == snake[0].y) {
			return true;
		}
	}
	return false;
}

// 定义生成食物函数
node creatFood(node* snake, int length) {
	node food;

	while (1) {
		food.x = rand() % (800 / NODE_WIDTH);
		food.y = rand() % (800 / NODE_WIDTH);

		int i;
		for (i = 0; i < length; i++) {
			if (snake[i].x == food.x && snake[i].y == food.y) {
				break;
			}
		}
		if (i < length) {
			continue;
		}
		else {
			break;
		}
	}
	return food;
}

// 定义绘制食物函数
void drawFood(node food) {
	int left, top, right, bottom;
	left = food.x * NODE_WIDTH;
	top = food.y * NODE_WIDTH;
	right = (food.x + 1) * NODE_WIDTH;
	bottom = (food.y + 1) * NODE_WIDTH;
	setfillcolor(YELLOW);
	solidrectangle(left, top, right, bottom);
	setfillcolor(WHITE);
}

int main() {
	srand((unsigned int)time(NULL));
	timeBeginPeriod(1);

	// 界面加载
	initgraph(1400, 800);
	setbkcolor(BLACK);
	cleardevice();

	// 图片对象创建
	//background image
	IMAGE imgBackground;
	loadimage(&imgBackground, "./BGLoad.jpg");
	// kun
	IMAGE imgkunHead;
	loadimage(&imgkunHead, "./kunHead.png");
	// kunMask
	IMAGE imgkunHeadMask;
	loadimage(&imgkunHeadMask, "./kunHeadMask.jpg");

	// 播放音乐
	mciSendString("open BKMStart.mp3", NULL, 0, NULL);
	mciSendString("play BKMStart.mp3", NULL, 0, NULL);

	Sleep(2000);
	// ********游戏加载界面********
	// 绘制游戏图标、名称及其边框
	setfillcolor(RGB(255, 128, 128));
	setlinecolor(RGB(255, 128, 128));
	fillroundrect(590, 291, 810, 509, 50, 50);
	setfillcolor(BLACK);
	fillroundrect(600, 301, 800, 499, 50, 50);
	putimage(623, 311, &imgkunHead);
	settextcolor(RGB(255, 128, 128));
	settextstyle(80, 0, _T("楷体"));
	setbkmode(TRANSPARENT);
	const char* GameName = "坤的考验";
	outtextxy(540, 550, GameName);

	Sleep(2000);

	cleardevice();

	//********贡献者名单********
	Sleep(1000);
	int right = 345;
	int step = 2;
	int frame = 0;
	int bearX = 1100;
	BeginBatchDraw();
	while (1) {
		cleardevice();
		putimage(0, 0, &imgBackground);
		// 名单
		settextcolor(RGB(128, 179, 255));
		settextstyle(50, 0, _T("楷体"));
		setbkmode(TRANSPARENT);
		const char* groupName = "练习时长两周半小组";
		outtextxy(250, 20, groupName);

		settextcolor(RGB(128, 179, 255));
		settextstyle(50, 0, _T("楷体"));
		setbkmode(TRANSPARENT);
		const char* Part1Name = "温家杰(测试)";
		outtextxy(250, 80, Part1Name);

		settextcolor(RGB(128, 179, 255));
		settextstyle(50, 0, _T("楷体"));
		setbkmode(TRANSPARENT);
		const char* Part2Name = "庄丹瑜(美工)";
		outtextxy(250, 140, Part2Name);

		settextcolor(RGB(128, 179, 255));
		settextstyle(50, 0, _T("楷体"));
		setbkmode(TRANSPARENT);
		const char* Part3Name = "蔡紫妍(美工)";
		outtextxy(250, 200, Part3Name);

		settextcolor(RGB(128, 179, 255));
		settextstyle(50, 0, _T("楷体"));
		setbkmode(TRANSPARENT);
		const char* Part4Name = "文斌伟(开发)";
		outtextxy(250, 260, Part4Name);

		settextcolor(RGB(128, 179, 255));
		settextstyle(50, 0, _T("楷体"));
		setbkmode(TRANSPARENT);
		const char* Part5Name = "周锦鹏(开发)";
		outtextxy(250, 320, Part5Name);

		// 加载条（白）
		setfillcolor(WHITE);
		setlinecolor(WHITE);
		fillroundrect(300, 600, 1000, 650, 50, 50);
		// 加载条（黑）
		setfillcolor(BLACK);
		setlinecolor(BLACK);
		fillroundrect(300, 600, right, 650, 50, 50);

		// 原地踏步的熊
		// load images
		IMAGE imgArrBearFrames[BEAR_FRAMES];
		for (int i = 0; i < BEAR_FRAMES; i++)
		{
			char strImgPath[100];
			sprintf(strImgPath, "bear/frames/bear%d.png", i);
			loadimage(&imgArrBearFrames[i], strImgPath);
		}

		// load masks
		IMAGE imgArrBearMasks[BEAR_FRAMES];
		for (int i = 0; i < BEAR_FRAMES; i++)
		{
			char strImgPath[100];
			sprintf(strImgPath, "bear/masks/bearmask%d.png", i);
			loadimage(&imgArrBearMasks[i], strImgPath);
		}

		putTransparentImage(bearX, 500, &imgArrBearMasks[frame], &imgArrBearFrames[frame]);

		FlushBatchDraw();
		/*right += rand() % step;*/
		right += step;
		Sleep(50);
		if (right >= 999) {
			break;
		}
		frame++;
		if (frame >= BEAR_FRAMES)
		{
			frame = 0;
		}
	}
	EndBatchDraw();

	// ********游戏界面********
	// 创建并设置背景颜色绘图窗口
	/*initgraph(1001, 801);*/
	setbkcolor(RGB(0, 0, 0));
	cleardevice();

	// 绘制网格
	//drawGrid();

	// 绘制会移动的蛇蛇
	node snake[20] = { {5, 7}, {4, 7}, {3, 7}, {2, 7} };
	int length = 4;
	enum direction d = down;

	// 生成食物
	node food = creatFood(snake, length);

	while (1) {
		// 生成
		cleardevice();
		drawSnake(snake, length);
		drawFood(food);
		Sleep(100);

		// 移动
		node lastTail = snakeMove(snake, length, d);
		changeDirection(&d);

		// 吃吃
		if (snake[0].x == food.x && snake[0].y == food.y) {
			if (length < 100) {
				snake[length] = lastTail;
				length++;
			}
			food = creatFood(snake, length);
		}

		// 结束
		if (judgmentGameOver(snake, length)) {
			break;
		}
	}
	// 播放音乐
	mciSendString("open WhatAreYouDoing.mp3", NULL, 0, NULL);
	mciSendString("play WhatAreYouDoing.mp3", NULL, 0, NULL);
	cleardevice();
	Sleep(3000);
	closegraph();


	timeEndPeriod(1);
	return 0;
}