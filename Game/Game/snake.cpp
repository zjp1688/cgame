//#include <easyx.h>
//#include <stdio.h>
//#include <conio.h>
//#include <time.h>
//
//// node结构体(蛇蛇和食物)
//typedef struct {
//	int x;
//	int y;
//}node;
//
//// 蛇蛇移动方向
//enum direction {
//	up,
//	down,
//	left,
//	right,
//};
//
//// 定义网格的宽度为20
//#define NODE_WIDTH 20
//
////// 定义绘制网格函数
////void drawGrid() {
////	int v = 0;
////	int r = 0;
////	for (v; v <= 1000; v += NODE_WIDTH) {
////		line(v, 0, v, 800);
////	}
////	for (r; r <= 800; r += NODE_WIDTH) {
////		line(0, r, 1000, r);
////	}
////}
//
//// 定义绘制蛇蛇函数
//void drawSnake(node* snake, int length) {
//	int left = 0;
//	int top = 0; 
//	int right = 0;
//	int bottom = 0;
//	setfillcolor(WHITE);
//	for (int i = 0; i < length; i++) {
//		left = snake[i].x * NODE_WIDTH;
//		top = snake[i].y * NODE_WIDTH;
//		right = (snake[i].x + 1) * NODE_WIDTH;
//		bottom = (snake[i].y + 1) * NODE_WIDTH;
//		solidrectangle(left, top, right, bottom);
//	}
//}
//
//// 定义蛇蛇移动函数
//node snakeMove(node* snake, int length, int direction) {
//	node tail = snake[length - 1];
//
//	// body
//	for (int i = length - 1; i > 0; i--) {
//		snake[i] = snake[i - 1];
//	}
//
//	// head
//	node newhead;
//	newhead = snake[0];
//	if (direction == left) {
//		newhead.x--;
//	}
//	else if (direction == right) {
//		newhead.x++;
//	}  
//	else if (direction == up) {
//		newhead.y--;
//	}
//	else {
//		newhead.y++;
//	}
//	snake[0] = newhead;
//	return tail;
//}
//
//// 定义控制蛇蛇方向函数
//void changeDirection(enum direction* d) {
//	if (_kbhit() != 0) {
//		char ch = _getch();
//		switch (ch) {
//		case 'w':
//			if (*d != up && *d != down) {
//				*d = up;
//			}
//			break;
//		case 's':
//			if (*d != up && *d != down) {
//				*d = down;
//			}
//			break;
//		case 'a':
//			if (*d != right && *d != left) {
//				*d = left;
//			}
//		case 'd':
//			if (*d != right && *d != left) {
//				*d = right;
//			}
//		}
//
//	}
//}
//
//// 定义蛇蛇Over函数
//bool judgmentGameOver(node* snake, int length) {
//	// border
//	if (snake[0].x <= 0 || snake[0].x >= (1000 / NODE_WIDTH) + 1) {
//		return true;
//	}
//	if (snake[0].y <= 0 || snake[0].y >= (800 / NODE_WIDTH) + 1) {
//		return true;
//	}
//
//	// body
//	for (int i = 1; i < length; i++) {
//		if (snake[i].x == snake[0].x && snake[i].y == snake[0].y) {
//			return true;
//		}
//	}
//	return false;
//}
//
//// 定义生成食物函数
//node creatFood(node* snake, int length) {
//	node food;
//
//	while (1) {
//		food.x = rand() % (800 / NODE_WIDTH);
//		food.y = rand() % (800 / NODE_WIDTH);
//
//		int i;
//		for (i = 0; i < length; i++) {
//			if (snake[i].x == food.x && snake[i].y == food.y) {
//				break;
//			}
//		}
//		if (i < length) {
//			continue;
//		}
//		else {
//			break;
//		}
//	}
//	return food;
//}
//
//// 定义绘制食物函数
//void drawFood(node food) {
//	int left, top, right, bottom;
//	left = food.x * NODE_WIDTH;
//	top = food.y * NODE_WIDTH;
//	right = (food.x + 1) * NODE_WIDTH;
//	bottom = (food.y + 1) * NODE_WIDTH;
//	setfillcolor(YELLOW);
//	solidrectangle(left, top, right, bottom);
//	setfillcolor(WHITE);
//}
//
//int main() {
//	srand((unsigned int)time(NULL));
//	// 创建并设置背景颜色绘图窗口
//	initgraph(1001, 801);
//	setbkcolor(RGB(0, 0, 0));
//	cleardevice();
//
//	// 绘制网格
//	//drawGrid();
//
//	// 绘制会移动的蛇蛇
//	node snake[20] = { {5, 7}, {4, 7}, {3, 7}, {2, 7} };
//	int length = 4;
//	enum direction d = right;
//
//	// 生成食物
//	node food = creatFood(snake, length);
//
//	while (1) {
//		// 生成
//		cleardevice();
//		drawSnake(snake, length);
//		drawFood(food);
//		Sleep(100);
//
//		// 移动
//		node lastTail = snakeMove(snake, length, d);
//		changeDirection(&d);
//
//		// 吃吃
//		if (snake[0].x == food.x && snake[0].y == food.y) {
//			if (length < 100) {
//				snake[length] = lastTail;
//				length++;
//			}
//			food = creatFood(snake, length);
//		}
//
//		// 结束
//		if (judgmentGameOver(snake, length)) {
//			break;
//		}
//	}
//
//	closegraph();
//	return 0;
//}